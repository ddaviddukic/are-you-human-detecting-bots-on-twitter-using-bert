---
title: "TAR"
author: "Dominik Stipić"
date: "March 25, 2020"
output: html_document
---

```{r}
library(stringr)
library(dplyr)
library(ggplot2)
library(gridExtra)
options(tz="CA")
```


```{r}
df <- read.csv(file="./indicator_data.tsv", sep = "\t")
df %>% select(X,prob_male, prob_female, prob_bot) -> X

male <- X %>% select(X, prob_male) %>% mutate(prob = prob_male, type="male")%>% select(X,prob,type)
female <- X %>% select(X, prob_female) %>% mutate(prob = prob_female, type="female")%>% select(X,prob,type)
bot <- X %>% select(X, prob_bot) %>% mutate(prob = prob_bot, type="bot") %>% select(X,prob,type)

Y <- rbind(male, female)
Y <- rbind(Y, bot)
```

```{r}
ps <- c()
for(p in Y$prob){
  p <- str_replace(p, ",", ".")
  ps <- c(ps, p);
}
cat(ps)
Y$prob <- as.numeric(ps)
```

```{r}

Y$type <- factor(Y$type, levels = c("bot", "male", "female"))

Y %>% ggplot(aes(x=reorder(X, prob), y=prob, fill=type)) + 
  geom_bar(stat = "identity", position = "dodge") +
  labs(y = "P(Indicator|Class)", x="Indicators", fill="Class") +
  theme_bw() + 
  scale_y_continuous(limit=c(0,0.8)) + 
  theme(axis.title.y = element_text(face = "bold", size=15), 
        axis.title.x = element_text(face="bold", size=15), 
        axis.text.x = element_text(face="bold", size = 15), 
        axis.text.y = element_text(face="bold", size = 10), 
        legend.background = element_rect(fill="lightblue",
                                         size=0.5, linetype="solid", 
                                         colour ="darkblue"),
        legend.text = element_text(face="bold", size=10),
        legend.title = element_text(face = "bold", size=10), 
        legend.position = c(0.2, 0.75)) -> fig
fig + ggsave(filename = "./plots/indicators_conference.png", width = 7, height = 5)
```

---
title: "TAR"
authors: "Dominik Stipić, David Dukić"
date: "March 25, 2020"
output: html_document
---

```{r}
library(stringr)
library(dplyr)
library(ggplot2)
library(gridExtra)
library(patchwork)
```

```{r}
bot_freq <- read.csv("./bot_freq.csv")
human_freq <- read.csv("./human_freq.csv")
topics <- read.csv("./topic_lda.csv")


bot_freq$X <- NULL
human_freq$X <- NULL
topics$X <- NULL

head(bot_freq)
head(human_freq)
head(topics)

names(bot_freq)[2] <- "bot_freq"
names(human_freq)[2] <- "human_freq"

K <- nrow(bot_freq)
```

```{r}
df <- inner_join(human_freq, bot_freq, by = c("word", "word"))
head(df)
df %>% mutate(human_p = human_freq/(human_freq + bot_freq), bot_p = bot_freq/(human_freq + bot_freq)) -> mle
df %>% mutate(human_p = (human_freq+1)/(human_freq + bot_freq+nrow(df)), bot_p = (bot_freq+1)/(human_freq + bot_freq+nrow(df))) -> map
head(mle)
head(map)
```

```{r}
get_top_words <- function(t, k){
  topics %>% filter(topic == t) %>% arrange(desc(prob)) %>% head(k) %>% select(word) -> X;
  X
}

get.plot.1 <- function(df, topic_i, k, y){
  top.words <- get_top_words(topic_i, k);
  X  <- inner_join(top.words, df, by = c("word", "word"));
  
  X %>% select(word, human_p) %>% mutate(prob = human_p, type="human") %>% select(word, prob, type)-> hs;
  X %>% select(word, bot_p) %>% mutate(prob = bot_p, type="bot") %>% select(word,prob, type) -> bs;
  probs <- rbind(hs,bs);
  
  probs %>% ggplot(aes(x = word, y = prob, fill = type)) + 
    geom_bar(stat="identity", position = position_dodge()) + 
    scale_y_continuous(limit = c(0, y)) + 
    labs(y = "P(Class|Word)", x="Words", fill="Class") +
    theme_bw() +
    theme(axis.text.x = element_text(size = 16, color="black", angle=90, vjust=0.3), 
          axis.text.y = element_text(size = 10, color="black"), 
          axis.title = element_text(size=15, face="bold") ,
          legend.background = element_rect(fill="lightblue",
                                           size=0.5, linetype="solid", 
                                           colour ="darkblue"),
          legend.position = c(0.15, 0.75),
          legend.text = element_text(face="bold", size=15),
          legend.title = element_text(face = "bold", size=15)) -> plt;
  #      legend.position = "none" ) -> plt
  plt
}

get.plot.2 <- function(df, topic_i, k, y){
  top.words <- get_top_words(topic_i, k);
  X  <- inner_join(top.words, df, by = c("word", "word"));
  
  X %>% select(word, human_p) %>% mutate(prob = human_p, type="human") %>% select(word, prob, type)-> hs;
  X %>% select(word, bot_p) %>% mutate(prob = bot_p, type="bot") %>% select(word,prob, type) -> bs;
  probs <- rbind(hs,bs);
  
  probs %>% ggplot(aes(x = word, y = prob, fill = type)) + 
    geom_bar(stat="identity", position = position_dodge()) + 
    scale_y_continuous(limit = c(0, y)) + 
    labs(x="Words") +
    theme_bw() +
    theme(axis.text.x = element_text(size = 16, color="black", angle=90, vjust=0.3),
          axis.title.x = element_text(size=15, face="bold"),
          axis.title.y = element_blank(), 
          axis.text.y = element_blank(),
          axis.ticks.y = element_blank(),
          legend.position = "none" )-> plt;
  plt
}
```

```{r}
k = 15
(get.plot.1(map, 3, k, .18) + get.plot.2(map, 1, k, .18)) + plot_layout(nrow = 1, ncol = 2, guides = "collect") -> plot

ggsave("./plots/bussines_life_map.png", plot = plot, width = 7, height = 4)

k = 20

get.plot.1(map, 1, k, .18) + ggsave("./plots/life_map_conference.png", width = 7, height = 5)

get.plot.1(map, 2, k, .18) + ggsave("./plots/politics_map_conference.png", width = 7, height = 5)

get.plot.1(map, 3, k, .18) + ggsave("./plots/bussines_map_conference.png", width = 7, height = 5)
```
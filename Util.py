# Created by ddukic on 27. 03. 2020.
import emoji
import string
import re
import spacy
from textblob import TextBlob
import numpy as np
from nltk.corpus import wordnet
from xml.sax.saxutils import unescape
import pandas as pd

english_vocab = set(w.lower() for w in wordnet.words())
wn_lemmas = set(wordnet.all_lemma_names())

nlp = spacy.load('en_core_web_lg')
punctuation = list(string.punctuation)
punctuation += ['’', '…', '-', '...', '”', '“', '–', '»', '«']
punctuation = ''.join(punctuation)
punctuation = punctuation.replace("'", '')

html_escape_table = {
    '&amp;': '&',
    '&quot;': '"',
    '&apos;': "'",
    '&gt;': '>',
    '&lt;': '<',
}


def htmlEscape(text):
    return unescape(text, html_escape_table)


def findEmojiInTweet(tweet):
    emojis = set()
    for char in tweet:
        if char in emoji.UNICODE_EMOJI:
            emojis.add(char)
    return emojis


def preprocessTweet(tweet):
    # remove emoji
    tweet = emoji.get_emoji_regexp().sub(u'', tweet)
    # remove \n if there is any
    tweet = tweet.strip()
    # remove RT
    if tweet.startswith("RT "):
        tweet = tweet[3:]
    # all characters to lowercase
    tweet = tweet.lower()
    # remove links
    tweet = re.sub(r'((www\.[^\s]+)|(https?://[^\s]+)|(http?://[^\s]+))', '', tweet)
    tweet = re.sub(r'http\S+', '', tweet)
    # remove username
    tweet = re.sub(r'@[^\s]+', '', tweet)
    # remove possible mails
    tweet = re.sub(r'([A-Za-z0-9+-._]+@[A-Za-z0-9+-._]+\.[A-Za-z0-9+-._]{2,})', '', tweet)
    # remove # in #hashtag
    tweet = re.sub(r'#([^\s]+)', r'\1', tweet)
    # remove end of tweet if tweet ends in …
    tweet = re.sub(r'(.*)[\s]+.*…$', r'\1', tweet)
    # convert HTML special characters to punctuation
    tweet = htmlEscape(tweet)
    # replace ` with '
    tweet = tweet.replace(u'’', u"'")
    # remove single quote if it is not in the middle of word
    tweet = re.sub(r"(?!\b'\b)'", '', tweet)
    # remove numbers
    tweet = tweet.translate(str.maketrans('', '', string.digits))
    # remove html tags e.g. </dskdkaks> or <asdasdk/>
    tweet = re.sub('&lt;/?[a-z]+&gt;', '', tweet)
    # remove punctuation
    tweet = tweet.translate(str.maketrans(punctuation, ' ' * len(punctuation)))
    # remove unnecessary whitespaces
    tweet = ' '.join(tweet.split())
    # correct grammar errors
    # tweet = str(TextBlob(tweet).correct())
    return tweet


def spacyTokenizerLemmatizer(tweet):
    # tokenize, lemmatize and remove stopwords
    return [word.lemma_.lower().strip() for word in nlp(tweet, disable=['parser', 'ner']) if
            word.lemma_ != '-PRON-' and word.lemma_ in wn_lemmas and not word.is_stop and len(word.lemma_) > 1]


def identity_tokenizer(tweets):
    return tweets


def loadDatasetForGC(path):
    classes = ['male', 'female']
    X, y = [], []
    with open(path, 'r', encoding='utf-8') as dataset:
        lines = dataset.readlines()
        for line in lines:
            if line.split("\t")[3] != "bot":
                X.append(preprocessTweet(line.split("\t")[1].strip()))
                y.append(classes.index(line.split("\t")[4].strip()))
    return np.array(X), np.array(y)


def loadDatasetForGCPreprocess(path):
    classes = ['male', 'female']
    X, y = [], []
    with open(path, 'r', encoding='utf-8') as dataset:
        lines = dataset.readlines()
        for line in lines:
            if line.split("\t")[3] != "bot":
                lemmas = spacyTokenizerLemmatizer(line.split("\t")[1].strip())
                if lemmas:
                    X.append(lemmas)
                    y.append(classes.index(line.split("\t")[4].strip()))
    return np.array(X), np.array(y)


def createWordVectorFeatures(path):
    df = pd.read_csv(path, encoding='utf-8', header=0, names=['tweet', 'class'])
    gender_dataset = df[df['class'] != 'bot']
    classes = ['male', 'female']
    X, y = [], []
    for ind in gender_dataset.index:
        tweet = gender_dataset['tweet'][ind]
        gender = gender_dataset['class'][ind]
        tokens = nlp(tweet)
        vectors = [token.vector for token in tokens if len(token.text) > 1]
        features = np.zeros(300)
        for vector in vectors:
            features += vector
        if np.count_nonzero(features) > 0:
            X.append(features)
            y.append(classes.index(gender))
    return np.array(X), np.array(y)

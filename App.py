# Created by ddukic on 20. 10. 2020.
from tkinter import *
import tkinter.font as tk_font
import transformers
import torch
import pandas as pd
import numpy as np
import gensim.models as gsm
import emoji
import re
import regex
import pickle
from xml.sax.saxutils import unescape
from sklearn.pipeline import Pipeline
from sklearn.base import BaseEstimator


class EmojiRemover(BaseEstimator):
    def fit(self, x, y=None):
        return self

    def transform(self, tweet):
        tweet = emoji.get_emoji_regexp().sub(u' ', tweet)
        tweet = ' '.join(tweet.split())
        return tweet


class RegexFilter(BaseEstimator):
    def __init__(self, regex):
        self.reg = regex

    def fit(self, x, y=None):
        return self

    def find(self, sentence):
        xs = re.findall(self.reg, sentence)
        return xs

    def transform(self, sentence):
        patterns = self.find(sentence)
        for pattern in patterns:
            sentence = sentence.replace(pattern, " ")
        return sentence


class DigitRemover(BaseEstimator):
    def fit(self, x, y=None):
        return self

    def transform(self, sentence):
        result = ''.join([i for i in sentence if not i.isdigit()])
        return result


class HTMLRemover(BaseEstimator):
    def __init__(self):
        self.html_escape_table = {'&amp;': '&',
                                  '&quot;': '"',
                                  '&apos;': "'",
                                  '&gt;': '>',
                                  '&lt;': '<'}

    def fit(self, x, y=None):
        return self

    def transform(self, text):
        return unescape(text, self.html_escape_table)


class ToLowerJoin(BaseEstimator):
    def fit(self, x, y=None):
        return self

    def transform(self, sentence):
        result = ' '.join(sentence.lower().split())
        return result


class OneLetterRemover(BaseEstimator):
    def fit(self, x, y=None):
        return self

    def transform(self, sentence):
        result = ' '.join([word.strip() for word in sentence.split() if len(word) > 1 or word == 'a' or word == 'i'])
        return result.strip()


class ApostropheReplacer(BaseEstimator):
    def fit(self, x, y=None):
        return self

    def transform(self, sentence):
        result = sentence.replace(u'’', u"'")
        return result


class ApostropheRemover(BaseEstimator):
    def fit(self, x, y=None):
        return self

    def transform(self, sentence):
        result = re.sub(r"(?!\b'\b)'", '', sentence)
        return result


path = './models/'
e2v = gsm.KeyedVectors.load_word2vec_format(path + 'emoji2vec.bin', binary=True)

model_bh = pickle.load(open(path + 'lr_tweet_emoji_categories_bot_human.sav', 'rb'))
model_mf = pickle.load(open(path + 'lr_tweet_emoji_categories_male_female.sav', 'rb'))

model_class, tokenizer_class, pretrained_weights = (
    transformers.BertModel, transformers.BertTokenizer, 'bert-base-uncased')

tokenizer = tokenizer_class.from_pretrained(pretrained_weights)
bert_model = model_class.from_pretrained(pretrained_weights)

url_regex = 'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
mention_regex = "@\w+"
hashtag_regex = "#\w+"
number_regex = ".[0-9]+."
punc_regex = "[!\"#$%&\\\\()“”*…+,-./:;<=>?@\[\]^_`{|}~–»«]+"
rt_regex = "RT"

cleaner = Pipeline([
    ("apostrophe_replacer", ApostropheReplacer()),
    ("url_cleaner", RegexFilter(url_regex)),
    ("mention_cleaner", RegexFilter(mention_regex)),
    ("hashtag_cleaner", RegexFilter(hashtag_regex)),
    ("rt_cleaner", RegexFilter("RT")),
    ("ft_cleaner", RegexFilter("FT")),
    ("number_cleaner", DigitRemover()),
    ("emoji_filter", EmojiRemover()),
    ("html_remover", HTMLRemover()),
    ("apostrophe_remover", ApostropheRemover()),
    ("punc_cleaner", RegexFilter(punc_regex)),
    ("dots_remover", RegexFilter("…")),
    ("to_lower_join", ToLowerJoin()),
    ("one_letter_remove", OneLetterRemover()),
    ("punc_cleaner_2", RegexFilter(punc_regex)),
    ("one_letter_remove2", OneLetterRemover())])


def find_emoji(tweet):
    emoji_set = set()
    data = regex.findall(r'\X', tweet)
    for word in data:
        if any(char in emoji.UNICODE_EMOJI for char in word):
            emoji_set.add(word)
    return emoji_set


def create_emoji_features(em):
    vector_sum = np.zeros(300)
    for char in emoji.get_emoji_regexp().split(em.strip()):
        if char.strip():
            try:
                vector = e2v[char]
            except:
                pass
            else:
                vector_sum += vector
    return vector_sum


def extract_features(df):
    with torch.no_grad():
        tweet = df['tweet'][0]
        emoji = df['emoji'][0]
        RT = np.array([float(df['RT'][0])])
        url = np.array([float(df['url'][0])])
        mention = np.array([float(df['mention'][0])])
        hashtag = np.array([float(df['hashtag'][0])])

        # if tweet is nan
        if tweet != tweet:
            emoji_features = create_emoji_features(emoji)
            return np.concatenate((np.zeros(768), emoji_features, RT, url, mention, hashtag), axis=0)

        encoded_dict = tokenizer.encode_plus(
            tweet,
            add_special_tokens=True,
            max_length=50,
            pad_to_max_length=True,
            return_attention_mask=True,
            return_tensors='pt')

        input_id = torch.cat([encoded_dict['input_ids']], dim=0)
        attention_mask = torch.cat([encoded_dict['attention_mask']], dim=0)

        last_hidden_state = bert_model(input_id, attention_mask=attention_mask)
        feature_vector = last_hidden_state[0][:, 0, :].cpu().data.numpy()[0]

        if emoji != 0:
            emoji_features = create_emoji_features(emoji)
            return np.concatenate((feature_vector, emoji_features, RT, url, mention, hashtag), axis=0)
        else:
            return np.concatenate((feature_vector, np.zeros(300), RT, url, mention, hashtag), axis=0)


def preprocess(tweet):
    df = pd.DataFrame(columns=['tweet'])
    df.loc[0] = [tweet]
    df['RT'] = df.apply(lambda x: 1 if len(re.findall(rt_regex, x['tweet'])) > 0 else 0, axis=1)
    df['url'] = df.apply(lambda x: 1 if len(re.findall(url_regex, x['tweet'])) > 0 else 0, axis=1)
    df['mention'] = df.apply(lambda x: 1 if len(re.findall(mention_regex, x['tweet'])) > 0 else 0, axis=1)
    df['hashtag'] = df.apply(lambda x: 1 if len(re.findall(hashtag_regex, x['tweet'])) > 0 else 0, axis=1)
    df['emoji'] = df.apply(lambda x: ''.join(find_emoji(x['tweet'])) if len(find_emoji(x['tweet'])) > 0 else 0, axis=1)
    df.loc[0, 'tweet'] = cleaner.fit_transform(df.loc[0]['tweet'].strip())
    return df


def predict(tweet, model, classes):
    features = extract_features(preprocess(tweet)).reshape(1, -1)
    class_ = model.predict(features)[0]
    prediction = classes[class_]
    proba = str(round(model.predict_proba(features)[0][class_] * 100, 2))
    return prediction, proba


class BotApp(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.master = master
        self.master.title("")
        default_font = tk_font.Font(family='Arial', size=10, weight=tk_font.BOLD)

        self.label_bot_detection = Label(root, text="BOT DETECTION", font=default_font)
        self.bot_tweet_window = Entry(master, state='normal', width=110)  # Text(master, height=2, state='normal')
        self.bot_button = Button(master, text="DETECT", command=self.predict_bot, font=default_font, bg='green')
        self.label_bot_result = Label(root, text="", font=default_font)
        self.clear_bot_button = Button(master, text="CLEAR", command=self.clear_bot, font=default_font, bg='red')

        self.label_gender_prediction = Label(root, text="GENDER PREDICTION", font=default_font)
        self.gender_tweet_window = Entry(master, state='normal', width=110)  # Text(master, height=2, state='normal')
        self.gender_button = Button(master, text="PREDICT", command=self.predict_gender, font=default_font, bg='green')
        self.label_gender_result = Label(root, text="", font=default_font)
        self.clear_gender_button = Button(master, text="CLEAR", command=self.clear_gender, font=default_font, bg='red')

        self.label_bot_detection.grid(column=0, row=0)
        self.bot_tweet_window.grid(column=0, row=1)
        self.bot_button.grid(column=1, row=1)
        self.clear_bot_button.grid(column=1, row=2)
        self.label_bot_result.grid(column=0, row=2)

        self.label_gender_prediction.grid(column=0, row=5)
        self.gender_tweet_window.grid(column=0, row=6)
        self.gender_button.grid(column=1, row=6)
        self.clear_gender_button.grid(column=1, row=7)
        self.label_gender_result.grid(column=0, row=7)

    def predict_bot(self):
        tweet = self.bot_tweet_window.get()
        if len(tweet) == 0:
            self.label_bot_result.config(text="Enter tweet and try again!", fg='red')
        else:
            classes = ['bot', 'human']
            prediction, proba = predict(tweet, model_bh, classes)
            self.label_bot_result.config(text="predicted: " + str(prediction) + "\nconfidence: " + str(proba) + "%",
                                         fg='blue')

    def predict_gender(self):
        tweet = self.gender_tweet_window.get()
        if len(tweet) == 0:
            self.label_gender_result.config(text="Enter tweet and try again!", fg='red')
        else:
            classes = ['female', 'male']
            prediction, proba = predict(tweet, model_mf, classes)
            self.label_gender_result.config(text="predicted: " + str(prediction) + "\nconfidence: " + str(proba) + "%",
                                            fg='blue')

    def clear_bot(self):
        self.bot_tweet_window.delete(0, END)
        self.label_bot_result.config(text='', fg='blue')

    def clear_gender(self):
        self.gender_tweet_window.delete(0, END)
        self.label_gender_result.config(text='', fg='blue')


root = Tk()
gui = BotApp(root)
root.mainloop()

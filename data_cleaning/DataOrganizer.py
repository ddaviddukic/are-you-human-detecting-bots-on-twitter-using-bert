# Created by ddukic on 24. 03. 2020.
import os
import xml.etree.ElementTree as ET
from collections import defaultdict


def parseDirectory(path, truth_path, output_path):
    data = os.listdir(path)

    author_tweet = defaultdict(list)

    with open(truth_path) as truth:
        lines = truth.readlines()
        for line in lines:
            ID, human_bot, gender = line.split(":::")
            author_tweet[ID].append(human_bot.strip())
            author_tweet[ID].append(gender.strip())

    with open(output_path, 'a') as output:
        for file in data:
            if file.split(".")[1] == "xml":
                ID = file.split(".")[0]
                tree = ET.parse(path + file)
                root = tree.getroot()
                for documents in root:
                    for tweet in documents:
                        tweet_content = ' '.join(tweet.text.split())
                        if tweet_content.startswith("RT"):
                            output.write(ID + "\t" + tweet_content + "\t" + "RT" + "\t" + author_tweet[ID][0] + "\t" +
                                         author_tweet[ID][1] + "\n")
                        else:
                            output.write(ID + "\t" + tweet_content + "\t" + "TW" + "\t" + author_tweet[ID][0] + "\t" +
                                         author_tweet[ID][1] + "\n")


parseDirectory("../dataset/train/", "../dataset/train/truth-train.txt", "./train.tsv")
parseDirectory("../dataset/test/", "../dataset/test/truth-test.txt", "./test.tsv")

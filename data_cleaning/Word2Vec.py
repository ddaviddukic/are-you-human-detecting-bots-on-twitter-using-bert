# Created by ddukic on 02. 05. 2020.
from Util import nlp


def tweetToVectors(path_in, path_out):
    with open(path_in, 'r') as tweets:
        lines = tweets.readlines()
        words = set()
        for line in lines:
            tweets = line.split(',')[0].split()
            for tweet in tweets:
                words.add(tweet.strip())
    with open(path_out, 'w') as file:
        for word in words:
            token = nlp(word)
            vector = token.vector
            vector = ' '.join([str(float(value)) for value in token.vector])
            file.write(word + ',' + vector + '\n')


# tweetToVectors('./train_clean2.csv', './train_word2vec.csv')
# tweetToVectors('./test_clean2.csv', './test_word2vec.csv')



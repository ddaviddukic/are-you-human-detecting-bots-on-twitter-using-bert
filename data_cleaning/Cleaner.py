# Created by ddukic on 07. 04. 2020.
import spacy
from Util import preprocessTweet
from Util import nlp
from Util import wn_lemmas


def removeMultipleLanguages(path_in, path_out):
    print(len(nlp.vocab))
    with open(path_in, 'r', encoding='utf-8') as dataset:
        lines = dataset.readlines()
        clean_lines = list()
        for line in lines:
            parts = line.split('\t')
            tweet = preprocessTweet(parts[1])
            tweet_words = tweet.split()
            filtered_tweet = [token.text for token in nlp(tweet, disable=['parser']) if
                              token.lemma_ in nlp.vocab and not token.is_stop and len(token.text) > 1]
            if len(filtered_tweet) == len(tweet_words) and len(filtered_tweet) != 0:
                parts[1] = tweet
                clean_lines.append('\t'.join(parts))
            elif len(filtered_tweet) > 0:
                filtered_tweet = ' '.join(filtered_tweet)
                parts[1] = filtered_tweet
                clean_lines.append('\t'.join(parts))

    with open(path_out, 'w', encoding='utf-8') as dataset_clean:
        for line in clean_lines:
            dataset_clean.write(line)


removeMultipleLanguages("./train.tsv", "./train_clean.tsv")
removeMultipleLanguages("./test.tsv", "./test_clean.tsv")


# Text Analysis and Retrieval Course Project 2020
## Are You Human? Detecting Bots on Twitter Using BERT
#### Authors: David Dukić, Dominik Keča, Dominik Stipić
#### Course web page: https://www.fer.unizg.hr/en/course/taar
#### Task web page: https://pan.webis.de/clef19/pan19-web/author-profiling.html

* Repository contains source code and a paper for course project from subject Text Analysis and Retrieval at Faculty of Electrical Engineering and Computing, University of Zagreb, 2020
* Slightly modified and updated version of TAR project paper was accepted at the 2020 IEEE 7<sup>th</sup> International Conference on Data Science and Advanced Analytics (DSAA) and can be found in IEEE Xplore digital library: https://ieeexplore.ieee.org/document/9260074
* Data set was not included as it is not public
* To get access to the data set, one must contact the authors of the task

---
title: "TAR"
author: "Dominik Stipić"
date: "March 25, 2020"
output: html_document
---

```{r}
library(stringr)
library(dplyr)
library(ggplot2)
library(gridExtra)

```


```{r}
df <- read.csv(file="./data_cleaning/train.tsv", sep = "\t", stringsAsFactors = F)
as_tibble(df) -> df
```

```{r}
head(df)
```



```{r}
df %>% mutate(letters = substr(message, 1, 3), value = str_detect(message,"^[A-Z][A-Z] .*")) -> X
```


```{r}
X %>% filter(ime == TRUE) %>% select(letters) %>% unique %>% unlist
```
```{r}
head(df) -> Y
Y$message

```

```{r}
f <- function(row){
  N <- length(row)
  if(row[N] == T){
    row[2] <- str_sub(row[2], start = 4, end = str_length(row[2]))
  }
  row
}

X -> Z
for(i in 1:nrow(Z)){
  xs <- Z[i,]
  Z[i,] = f(xs)
}
Z
```

```{r}
Z$value <- NULL
Z$letters <- NULL
```

```{r}
write.csv2(Z, file = "./a.csv")
```


```{r}
df <- read.csv(file="./data_cleaning/test.tsv", sep = "\t", stringsAsFactors = F)
as_tibble(df) -> df
```


```{r}
df <- read.csv(file="./data_cleaning/test.tsv", sep = "\t", stringsAsFactors = F)
as_tibble(df) -> df

df %>% mutate(letters = substr(message, 1, 3), value = str_detect(message,"^[A-Z][A-Z] .*")) -> X

f <- function(row){
  N <- length(row)
  if(row[N] == T){
    row[2] <- str_sub(row[2], start = 4, end = str_length(row[2]))
  }
  row
}

X -> Z
for(i in 1:nrow(Z)){
  xs <- Z[i,]
  Z[i,] = f(xs)
}
Z
```



```{r}
df <- read.csv(file="../cleaned/corpus/abstracations", stringsAsFactors = F)
head(df)
```

#Senti

```{r}
b <- read.csv("../senti/bot.csv")
m <- read.csv(file="../senti/male.csv")
f <- read.csv(file="../senti/female.csv")
```

```{r}
b %>% gather(Emotion, Value, anger:trust) -> bs
f %>% gather(Emotion, Value, anger:trust) -> fs
m %>% gather(Emotion, Value, anger:trust) -> ms

hs <- rbind(ms,fs)
```

```{r}
bs %>% group_by(Emotion) %>% summarise(v = sum(Value)/nrow(b) * 100) -> B
fs %>% group_by(Emotion) %>% summarise(v = sum(Value)/nrow(f) * 100) -> F
ms %>% group_by(Emotion) %>% summarise(v = sum(Value)/nrow(m) * 100) -> M

B$Type <- "Bot"
F$Type <- "Female"
M$Type <- "Male"

X = rbind(B,F)
X = rbind(X,M)
X
```

```{r}
bot.words <- nrow(b)
human.words <- nrow(m) + nrow(f)
bs %>% group_by(Emotion) %>% summarise(v = sum(Value)/bot.words) -> B
hs %>% group_by(Emotion) %>% summarise(v = sum(Value)/human.words) -> H


B$Type <- "Bot"
H$Type <- "Human"

X = rbind(B,H)
X
```



```{r}
ggplot(X, aes(x = Emotion, y = v, fill = Type)) + 
geom_bar(stat = "identity", position="dodge") + 
labs(y = "Percent", fill="Emotion") +
theme(axis.text.x = element_text(angle = 90)) + 
  ggsave("./plots/human_bot.png")
```

```{r}
bs %>% group_by(Emotion) %>% summarise(v = sum(Value)/bot.words) -> B
hs %>% group_by(Emotion) %>% summarise(v = sum(Value)/human.words) -> H


B$Type <- "Bot"
H$Type <- "Human"

X = rbind(B,H)
X
```

```{r}
df <- read.csv(file="./train_data.csv")
df$X <- NULL
```

```{r}
library("tibble")
df <- as_tibble(df)
```

```{r}
df
```

```{r}
df %>% filter(y == "male") -> male
df %>% filter(y == "female") -> female
df %>% filter(y == "bot") -> bot
```

```{r}

for (x in head(df)$tweet){
  cat(x);
  }
```

```{r}
bot <- read.csv(file="../senti/bot_senti.csv")
female <- read.csv(file="../senti/female_senti.csv")
male <- read.csv(file="../senti/male_senti.csv")

bot$X <- NULL
female$X <- NULL
male$X <- NULL

bot$Words <- NULL
female$Words <- NULL
male$Words <- NULL

head(bot)
human <- rbind(male, female)
head(human)

bot.words <- nrow(bot)
human.words <- nrow(human)
```

```{r}
bot %>% gather(Emotion, Value, anger:Charged) -> bs
human %>% gather(Emotion, Value, anger:Charged) -> hs
hs
```

```{r}
bs %>% group_by(Emotion) %>% summarise(v = sum(Value)/bot.words) -> B
hs %>% group_by(Emotion) %>% summarise(v = sum(Value)/human.words) -> H


B$Type <- "Bot"
H$Type <- "Human"

X = rbind(B,H)
X
```

```{r}
ggplot(X, aes(x = Emotion, y = v, fill = Type)) + 
geom_bar(stat = "identity", position="dodge") + 
labs(y = "Percent", fill="Emotion") +
theme(axis.text.x = element_text(angle = 90))
```

```{r}
bot <- read.csv("../lda/mle/bot_lda.csv")
human <- read.csv("../lda/mle/human_lda.csv")
topics <- read.csv("../lda/mle/topic_lda.csv")
head(bot)
head(human)
head(topics)

bot$X <- NULL
human$X <- NULL
topics$X <- NULL

bot$type <- "bot"
human$type <- "human"
categories <- rbind(bot, human)

categories %>% filter(word == "ability")
```

```{r}
get_top_words <- function(t, k){
  topics %>% filter(topic == t) %>% arrange(desc(prob)) %>% head(k) %>% select(word) -> X;
  X
}

get_plots <- function(topic_i, k){
  most_freq <- get_top_words(topic_i, k) %>% unlist;
  human_topic <- human %>% filter( word %in% most_freq);
  bot_topic <- bot %>% filter( word %in% most_freq);
  X <- rbind(human_topic, bot_topic);
  X %>% ggplot(aes(x = word, y = prob, fill = type)) + 
    geom_bar(stat="identity", position = "dodge") + 
    scale_y_continuous() + 
    labs(y = "probability", x="Words") +
    theme_bw() + 
    theme(axis.text.x = element_text(angle = 90))  -> plt;
  plt
}


get_top_words(1, 3)

```
```{r}
get_plots(3, 20) -> plt_business
get_plots(4,20) -> plt_life
get_plots(2,20) -> plt_politics
plt_business 
plt_life
plt_politics
```

```{r}
plt_business + ggsave("./plots/business_mle.png", width=7, height = 5)
plt_life + ggsave("./plots/life_mle.png", width=7, height = 5)
plt_politics +  ggsave("./plots/politics_mle.png", width=7, height = 5)
```

```{r}
bot_freq <- read.csv("../lda/bot_freq.csv")
human_freq <- read.csv("../lda/human_freq.csv")
topics <- read.csv("../lda/mle/topic_lda.csv")


bot_freq$X <- NULL
human_freq$X <- NULL
topics$X <- NULL

head(bot_freq)
head(human_freq)
head(topics)

names(bot_freq)[2] <- "bot_freq"
names(human_freq)[2] <- "human_freq"

K <- nrow(bot_freq)
```

```{r}
df <- inner_join(human_freq, bot_freq, by = c("word", "word"))
head(df)
```
```{r}
df %>% mutate(human_p = human_freq/(human_freq + bot_freq), bot_p = bot_freq/(human_freq + bot_freq)) -> mle
df %>% mutate(human_p = (human_freq+1)/(human_freq + bot_freq+nrow(df)), bot_p = (bot_freq+1)/(human_freq + bot_freq+nrow(df))) -> map
head(mle)
head(map)
```

```{r}
get_top_words <- function(t, k){
  topics %>% filter(topic == t) %>% arrange(desc(prob)) %>% head(k) %>% select(word) -> X;
  X
}

get.plot <- function(df, topic_i, k, y){
  top.words <- get_top_words(topic_i, k);
  X  <- inner_join(top.words, df, by = c("word", "word"));
  
  X %>% select(word, human_p) %>% mutate(prob = human_p, type="human") %>% select(word, prob, type)-> hs;
  X %>% select(word, bot_p) %>% mutate(prob = bot_p, type="bot") %>% select(word,prob, type) -> bs;
  probs <- rbind(hs,bs);
  
  probs %>% ggplot(aes(x = word, y = prob, fill = type)) + 
    geom_bar(stat="identity", position = position_dodge()) + 
    scale_y_continuous(limit = c(0, y)) + 
    labs(y = "P(Class|Word)", x="Words", fill="Class") +
    theme_bw() +
    theme(axis.title.y = element_text(face = "bold"), axis.title.x = element_text(face="bold")) + 
    theme(axis.text.x = element_text(angle = 90)) -> plt;
  plt
}


```

```{r}
get.plot(map, 1, 30) 
get.plot(map, 2, 30) 
get.plot(map, 3, 30) 
get.plot(map, 4, 30) 

```

```{r}
get.plot(mle, 1, 20)
get.plot(mle, 2, 20)
get.plot(mle, 3, 20)
get.plot(mle, 4, 20)
```
```{r}
k = 20
get.plot(map, 1, k, .18) + ggsave("./plots/life_map.png", width = 7, height = 5)
get.plot(map, 2, k, .18) + ggsave("./plots/politics_map.png", width = 7, height = 5)
get.plot(map, 3, k, .18) + ggsave("./plots/bussines_map.png", width = 7, height = 5)
get.plot(map, 4, k, .18) + ggsave("./plots/x_map.png")

get.plot(mle, 1, k, 1) + ggsave("./plots/life_mle.png", width = 7, height = 5)
get.plot(mle, 2, k, 1) + ggsave("./plots/politics_mle.png", width = 7, height = 5)
get.plot(mle, 3, k, 1) + ggsave("./plots/bussines_mle.png", width = 7, height = 5)
get.plot(mle, 4, k, 1) + ggsave("./plots/x_mle.png", width = 7, height = 5)
```
```{r}
get.plot(mle, 1, 30) -> g1
get.plot(mle, 2, 30) -> g2
get.plot(mle, 3, 30) -> g3
get.plot(mle, 4, 30) -> g4

lay <- rbind(c(1,1,1),
             c(2,3,2))


grid.arrange(g1, g2, g3, ncol=2, nrow=2, common.legend=TRUE) 
```


```{r}
df <- read.csv(file="../rmd/podatci.tsv", sep = "\t")
df %>% select(X,prob_male, prob_female, prob_bot) -> X

male <- X %>% select(X, prob_male) %>% mutate(prob = prob_male, type="male")%>% select(X,prob,type)
female <- X %>% select(X, prob_female) %>% mutate(prob = prob_female, type="female")%>% select(X,prob,type)
bot <- X %>% select(X, prob_bot) %>% mutate(prob = prob_bot, type="bot") %>% select(X,prob,type)

Y <- rbind(male, female)
Y <- rbind(Y, bot)
```

```{r}
ps <- c()
for(p in Y$prob){
  p <- str_replace(p, ",", ".")
  ps <- c(ps, p);
}
cat(ps)
Y$prob <- as.numeric(ps)
```

```{r}
write.csv(Y, file="a.csv")
```


```{r}


Y %>% ggplot(aes(x=X, y=prob, fill=type)) + 
  geom_bar(stat = "identity", position = "dodge") +
  labs(y = "P(Indicator|Class)", x="Indicators", fill="Class") +
  theme_bw() + 
  scale_y_continuous(limit=c(0,0.8)) + ggsave("./plots/emoji1.png") + 
  theme(axis.title.y = element_text(face = "bold"), 
        axis.title.x = element_text(face="bold"), 
        axis.text.x = element_text(face="bold", size = 10), 
        axis.text.y = element_text(face="bold", size = 10), 
        legend.background = element_rect(fill="lightblue",
                                         size=0.5, linetype="solid", 
                                         colour ="darkblue"),
        legend.text = element_text(face="bold"),
        legend.title = element_text(face = "bold"), 
        legend.position = c(0.1, 0.8)) -> fig
fig
```

```{r}
fig + ggsave("./plots/emoji0.png", height = 4, width = 5)
```